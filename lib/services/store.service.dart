
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

import 'package:flutter/material.dart';

class StoreService with ChangeNotifier {

  Firestore _store = Firestore.instance;

  Stream<QuerySnapshot> getFiles() {
    return _store.collection("archivos").snapshots();
  }

  Future<void> uploadFile(String filename, double filesize) {
    return _store.collection("archivos").add({
      'filename': filename,
      'filesize': filesize
    });
  }

}