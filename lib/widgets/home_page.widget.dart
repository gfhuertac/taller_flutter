import 'package:another_app/services/auth.service.dart';
import 'package:another_app/services/store.service.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'loading_circle.widget.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.user}) : super(key: key);

  final FirebaseUser user;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.email),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: Provider.of<StoreService>(context).getFiles(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return LoadingCircle();
          return ListView(
            padding: const EdgeInsets.all(5.0),
            children: snapshot.data.documents.map((data) => _createListItem(context, data)).toList(),
          );
        }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Provider.of<AuthService>(context).logout();
        },
        tooltip: 'Logout',
        child: Icon(Icons.exit_to_app),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _createListItem(BuildContext context, DocumentSnapshot data) {
    final record = Record.fromSnapshot(data);
      return Padding(
        key: ValueKey(record.filename),
        padding: const EdgeInsets.all(5.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.orange),
            borderRadius: BorderRadius.circular(2.0)
          ),
          child: ListTile(
            title: Text(record.filename),
            trailing: Text((record.filesize/1024).round().toString()),
            onTap: () => print(record),
          ),
        ),
      );
    }
  }
  
  class Record {
  
    final String filename;
    final int filesize;
    final DocumentReference reference;
  
    Record.fromMap(Map<String, Object> data, {this.reference}):
      assert(data['filename'] != null),
      assert(data['filesize'] != null),
      filename = data['filename'],
      filesize = data['filesize'];

    Record.fromSnapshot(DocumentSnapshot snapshot): 
      this.fromMap(snapshot.data, reference: snapshot.reference);

    @override
    String toString() => "Record<$filename:$filesize";
}
