import 'package:another_app/services/auth.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Firebase Login"),
        ),
        body: Container(
            padding: EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Text(
                    'Ingresa tus datos',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 20.0),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: "Email"),
                    onSaved: (value) => _email = value,
                  ),
                  SizedBox(height: 20.0),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(labelText: "Password"),
                    onSaved: (value) => _password = value,
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                      child: Text("INGRESAR"),
                      onPressed: () async {
                        // save the fields..
                        final form = _formKey.currentState;
                        form.save();

                        // Validate will return true if is valid, or false if invalid.
                        if (form.validate()) {
                          try {
                            FirebaseUser result = await Provider.of<AuthService>(context)
                              .loginUser(email: _email, password: _password);
                              print(result);
                          } on Exception catch(error) {
                            return showDialog(
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Error!'),
                                  content: Text('Error validando al usuario'),
                                  actions: <Widget>[
                                    FlatButton(
                                        child: Text('Cancel'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        })
                                  ],
                                );
                              },
                              context: context,
                            );
                          }
                        }
                      
                      }
                  ),
                ],
              ),
            )));
  }
}
