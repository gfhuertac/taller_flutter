import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'services/auth.service.dart';
import 'services/store.service.dart';
import 'widgets/login.widget.dart';
import 'widgets/home_page.widget.dart';
import 'widgets/loading_circle.widget.dart';

void main() => runApp(
  MultiProvider(
    providers: [
      ChangeNotifierProvider<AuthService>(builder: (_) => AuthService()),
      ChangeNotifierProvider<StoreService>(builder: (_) => StoreService()),
    ],
    child: MyApp(),
  ));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder(
        future: Provider.of<AuthService>(context).getUser(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // log error to console
            if (snapshot.error != null) {
              print("error");
              return Text(snapshot.error.toString());
            }
            // redirect to the proper page, pass the user into the
            // `HomePage` so we can display the user email in welcome msg     ⇐ NEW
            return snapshot.hasData
                ? MyHomePage(user: snapshot.data)
                : LoginPage();
          } else {
            // show loading indicator                                         ⇐ NEW
            return LoadingCircle();
          }
        },
      ),
    );
  }
}
